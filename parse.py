import os

from bs4 import BeautifulSoup
from os import listdir
from os.path import isfile, join

KEEP_ROOT_DIR = "/home/pi2/ownCloud/GoogleNotes/Keep"

def parse():
    keep_files = [f for f in listdir(KEEP_ROOT_DIR) if isfile(join(KEEP_ROOT_DIR, f))]
    for filename in keep_files:
        if filename .endswith(".html"):
            # TODO:
            # * get <br> and convert to \n (use .contents and iterate within the list)
            # * get color:       div.note - other classes are RED, BLUE etc....
            # * get images       div.attachments > find_all li.attr(src) base64 encoded image   - file:///home/pi2/ownCloud/GoogleNotes/Keep/fake%20tinder.html
            # * get labels       span.chip.label > span.label-name                              - file:///home/pi2/ownCloud/GoogleNotes/Keep/MyDay%20things.html
            # * get links:       chips > .chip.annotation.WEBLINK                               - file:///home/pi2/ownCloud/GoogleNotes/Keep/Myers%20Briggs%20-%20entj.html
            with open(os.path.join(KEEP_ROOT_DIR, filename)) as f:
                rv = {
                    'filename': filename,
                    'heading': '',
                    'title': '',
                    'content': '',
                    'list': [],
                    'attachments': [],
                    'archived': False,
                    # TODO:
                    'color': '',
                    'labels': [],
                }
                parsed_html = BeautifulSoup(f.read())

                heading_el = parsed_html.find('div', attrs={'class': 'heading'})
                if heading_el:
                    rv['heading'] = heading_el.get_text()

                title_el = parsed_html.find('div', attrs={'class': 'title'})
                if title_el:
                    rv['title'] = title_el.get_text()

                body_el = parsed_html.find('div', attrs={'class': 'content'})
                if body_el:
                    rv['content'] = body_el.get_text()

                list_el = parsed_html.find('ul', attrs={'class': 'list'})
                if list_el:
                    li_els = list_el.findChildren('li')
                    for li_el in li_els:
                        rvl = {
                            'checked': False,
                            'content': '',
                        }
                        li_bullet = li_el.find('span', attrs={'class': 'bullet'})
                        if li_bullet and li_bullet.text != "☐":
                            rvl['checked'] = True

                        li_content = li_el.find('span', attrs={'class': 'text'})
                        if li_content:
                            rvl['content'] = li_content.get_text()
                        rv['list'].append(rvl)

                archived_el = parsed_html.find('span', attrs={'class': 'archived'})
                if archived_el:
                    rv['archived'] = True

                print(rv)


if __name__ == "__main__":
    parse()